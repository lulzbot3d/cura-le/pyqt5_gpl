#!/bin/bash
set -x

# Files to keep (--exclude from rsync)
EXCLUDE="README-git-mirror make-pyqt5-git-history .git .gitignore"

git clone ssh://git@code.alephobjects.com/source/PyQt5_gpl.git
cd PyQt5_gpl

cat > README-git-mirror <<EOF
The upstream PyQT doesn't appear to use git/hg/svn/cvs or any other version tracker.
They do their releases by pushing a tarball.

This repo has a copy of each tarball and a corresponding git tag.

The script make-pyqt5-git-history will download each tarball and create a new git archive.
EOF

git add README-git-mirror

git commit -m "Add README-git-mirror for new git archive of PyQt5"

cp -p ../make-pyqt5-git-history .

git add make-pyqt5-git-history

git commit -m "Script that creates a git archive from PyQt5 tarballs"

cd ..

# Some versions use a dash, others an underscore and no "5"
for QT5VER in 5.0 5.0.1 5.1 5.1.1 5.2 5.2.1 5.3 5.3.1 5.3.2 5.4 5.4.1 5.4.2 5.5 5.5.1
do echo $QT5VER
	wget --continue https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-$QT5VER/PyQt-gpl-$QT5VER.tar.gz
	tar xf PyQt-gpl-$QT5VER.tar.gz
	rsync -ultav --delete-after --exclude="$EXCLUDE" PyQt-gpl-$QT5VER/* PyQt5_gpl/
	cd PyQt5_gpl/
	git add .
	git commit -a -m "New tarball $QT5VER"
	git tag $QT5VER
	cd ..
	rm -rf PyQt-gpl-$QT5VER
done

# Starting with 5.6 an underscore was used in the filename
for QT5VER in 5.6 5.7 5.7.1 5.8 5.8.1 5.8.2 5.9 5.9.1 5.9.2 5.10 5.10.1 5.11.2 5.11.3
do echo $QT5VER
	wget --continue https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-$QT5VER/PyQt5_gpl-$QT5VER.tar.gz
	tar xf PyQt5_gpl-$QT5VER.tar.gz
	rsync -ultav --delete-after --exclude="$EXCLUDE" PyQt5_gpl-$QT5VER/* PyQt5_gpl/
	cd PyQt5_gpl/
	git add .
	git commit -a -m "New tarball $QT5VER"
	git tag $QT5VER
	cd ..
	rm -rf PyQt5_gpl-$QT5VER
done

# If you want to push it, then:
# cd PyQt5_gpl/
# git push

